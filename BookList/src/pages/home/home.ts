import { Component } from '@angular/core';
import { NavController, AlertController, ActionSheetController } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  books: FirebaseListObservable<any>;
  constructor(public navCtrl: NavController, public alertCtrl: AlertController, aDB: AngularFireDatabase,
              public actionSheetCtrl: ActionSheetController) {
    this.books = aDB.list('/Books');
  }


  showOptions(bookId, bookTitle){
    let actionSheet = this.actionSheetCtrl.create({
      title:'What do you want to do?',
      buttons:[{
        text: 'Delete Book',
        role: 'destructive',
        handler: () => {
          this.removeBook(bookId);
        }
      },{
        text: 'Update title',
        handler: () =>{
          this.updateBook(bookId, bookTitle);
        }
      },{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel Clicked');
        }
      }
      ]
    });
    actionSheet.present();
  }

  removeBook(bookId: string){
    this.books.remove(bookId);
  }

  updateBook(bookId, bookTitle){
    let prompt = this.alertCtrl.create({
      title: 'Book Name',
      message: "Update the name for this book",
      inputs:[
        {
        name: 'title',
        placeholder: 'Title',
        value: bookTitle
        },
      ],
      buttons:[
        {
          text: 'Cancel',
          handler: data =>{
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            this.books.update(bookId, {
              title: data.title
            });
          }
        }
      ]
    });
    prompt.present();
  }


  addSong(){
    let prompt = this.alertCtrl.create({
      title: 'Book Name',
      message: "Enter a name for this new book and the author you're so keen on adding",
      inputs:[
        {
          name: 'title',
          placeholder: 'Title'
        },
        {
          name: 'author',
          placeholder: 'Author'
        }
      ],
      buttons:[
        {
          text:'Cancel',
          handler: data =>{
            console.log('Cancel Clicked');
          }
        },
        {
          text: 'Save',
          handler: data =>{
            this.books.push({
              title: data.title,
              author: data.author
            });
          }
        }
      ]
    });
    prompt.present();
  }
}
